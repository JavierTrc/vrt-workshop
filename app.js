const express = require('express');
const shell = require('shelljs');
const path = require('path')
const fs = require("mz/fs");

const getDiff = require('./utils/compare');
const getReports = require('./utils/reportsUtils').getReports;

const CYPRESS = path.normalize('./node_modules/.bin/cypress') 
const CYPRESS_RUN = path.normalize(CYPRESS + ' run')
const CYPRESS_SCREENSHOT_ROOT = path.normalize('./cypress/screenshots/screenshot_spec.js/')

var app = express();

var running = false;

app.use(express.static('public'))
app.set('view engine', 'pug')

app.get('/', function (req, res) {
    res.render('index', {reports: getReports()})
  })

app.post('/generate', async function(req, res) {
    if(running) {
        return res.json({running:true})
    } else {
        running = true;
    }
    var now = new Date();
    var reportDir = path.normalize(`./public/reports/${now.getTime()}`)
    res.json({running:true})
    var verify = shell.exec(`${CYPRESS} verify`).code
    if (verify != 0) {
        shell.exec(`${CYPRESS} install`);
    }
    fs.mkdir(reportDir)
    var runCode = shell.exec(CYPRESS_RUN).code
    if (runCode != 0) {
        fs.rmdir(reportDir);
    }
    shell.cp(CYPRESS_SCREENSHOT_ROOT + '*.png', reportDir)

    const data = await getDiff(now.getTime())
    await fs.writeFile(reportDir + '/data.json', JSON.stringify(data))
    running = false;
})

app.get('/update', function(req, res) {
    res.json({running: running});
})

var port = process.env.PORT || 8000

app.listen(port, () => console.log(`Listening on port ${port}`))