var intervalId = undefined;

function onClickGenerate() {

    if(intervalId) return;

    fetch('/generate', {
        method: 'POST'
    }).then( response => response.json())
    .then( running => {
        if(running.running) {
            $('button').toggleClass("loading disabled");
            console.log('timeout set')
            intervalId = setInterval(pollUpdate, 1000);
        }
    });
}

function pollUpdate() {
    fetch('/update', {
        method: 'GET'
    }).then( response => response.json())
    .then( running => {
        console.log('Entering poll ')
        if(!running.running) {
            console.log('Stopping running')
            location.replace('/')
            
        }
    });
}