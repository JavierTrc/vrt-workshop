const compareImages = require("resemblejs/compareImages");
const fs = require("mz/fs");

async function getDiff(report) {
    const options = {
        output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255
            },
            errorType: "flat",
            transparency: 1,
            largeImageThreshold: 1200,
            useCrossOrigin: false,
            outputDiff: true
        },
        scaleToSameSize: true,
        ignore: "less"
    };

    // The parameters can be Node Buffers
    // data is the same as usual with an additional getBuffer() function
    const data = await compareImages(
        await fs.readFile(`./public/reports/${report}/Before.png`),
        await fs.readFile(`./public/reports/${report}/After.png`),
        options
    );

    await fs.writeFile(`./public/reports/${report}/Diff.png`, data.getBuffer());

    return data;
}

module.exports = getDiff