const shell = require('shelljs');

const public = {};


public.getReports = function() {
    var reports = shell.ls('public/reports')
    return reports.map( (report) => {
        const data = shell.cat('public/reports/' + report + '/data.json')
        return {
            timestamp: (new Date(Number(report))).toLocaleString(),
            beforeImg: 'reports/' + report + '/Before.png',
            afterImg: 'reports/' + report + '/After.png',
            diffImg: 'reports/' + report + '/Diff.png',
            data: JSON.parse(data),
        }
    })
}

module.exports = public